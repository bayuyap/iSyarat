package smktelkom.learn.isyarat2;

import java.io.Serializable;

/**
 * Created by SMK TELKOM on 5/3/2018.
 */

public class Sekolah implements Serializable {
    public String judul, deskripsi, lokasi;
    public String foto;


    public Sekolah(String judul, String deskripsi, String lokasi, String foto) {
        this.judul = judul;
        this.deskripsi = deskripsi;
        this.lokasi = lokasi;
        this.foto = foto;
    }
}
