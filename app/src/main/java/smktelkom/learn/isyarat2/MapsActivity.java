package smktelkom.learn.isyarat2;

import android.content.ContentResolver;
import android.content.Intent;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import java.util.ArrayList;

public class MapsActivity extends AppCompatActivity implements Adapter.ISekolahAdapter {
    public static final String SEKOLAH = "sekolah";
    String goolgeMap = "com.google.android.apps.maps"; // identitas package aplikasi google masps android
    Uri gmmIntentUri;
    Intent mapIntent;
    String masjid_agung_demak = "-7.9657917,112.62846910000007"; // koordinat Masjid Agung Demak
    ArrayList<Sekolah> mList = new ArrayList<>();
    Adapter mAdapter;
    ArrayList<Sekolah> listall = new ArrayList<>();

    String mQuery;
    int itempos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        LinearLayoutManager manager = new LinearLayoutManager(this);
        RecyclerView rv = findViewById(R.id.Recycle);
        rv.setLayoutManager(manager);
        mAdapter = new Adapter(this, mList);
        rv.setAdapter(mAdapter);
        fillData();
        // menyamakan variable pada layout activity_main.xml


        // tombol untuk menjalankan navigasi goolge maps intents

    }

    @Override
    public void doClick(int pos) {

    }


    public void fillData() {
        Resources r = getResources();
        String[] jdl = r.getStringArray(R.array.places);
        String[] des = r.getStringArray(R.array.place_desc);
        String[] loc = r.getStringArray(R.array.place_locations);
        TypedArray a = r.obtainTypedArray(R.array.places_picture);
        String[] foto = new String[a.length()];
        for (int i = 0; i < foto.length; i++) {
            int id = a.getResourceId(i, 0);
            foto[i] = ContentResolver.SCHEME_ANDROID_RESOURCE + "://"
                    + r.getResourcePackageName(id) + '/'
                    + r.getResourceTypeName(id) + '/'
                    + r.getResourceEntryName(id);
        }
        a.recycle();
        for (int i = 0; i < jdl.length; i++) {
            mList.add(new Sekolah(jdl[i], des[i],
                    loc[i], foto[i]));
        }
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void doShare(int pos) {
        // Buat Uri dari intent string. Gunakan hasilnya untuk membuat Intent.
        gmmIntentUri = Uri.parse("google.navigation:q=" + masjid_agung_demak);

        // Buat Uri dari intent gmmIntentUri. Set action => ACTION_VIEW
        mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);

        // Set package Google Maps untuk tujuan aplikasi yang di Intent yaitu google maps
        mapIntent.setPackage(goolgeMap);

        if (mapIntent.resolveActivity(getPackageManager()) != null) {
            startActivity(mapIntent);
        } else {
            Toast.makeText(MapsActivity.this, "Google Maps Belum Terinstal. Install Terlebih dahulu.",
                    Toast.LENGTH_LONG).show();
        }
    }


}
