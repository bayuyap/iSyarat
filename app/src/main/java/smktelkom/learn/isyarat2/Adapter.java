package smktelkom.learn.isyarat2;

import android.content.Context;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by SMK TELKOM on 2/13/2018.
 */

public class Adapter extends RecyclerView.Adapter<Adapter.ViewHolder> {
    ArrayList<Sekolah> sekolahList;
    ISekolahAdapter mISekolahAdapter;

    public Adapter(Context context, ArrayList<Sekolah> sekolahList) {

        this.sekolahList = sekolahList;
        mISekolahAdapter = (ISekolahAdapter) context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v =
                LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list, parent,
                        false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Sekolah sekolah = sekolahList.get(position);
        holder.tvJudul.setText(sekolah.judul);
        holder.ivFoto.setImageURI(Uri.parse(sekolah.foto));
    }


    @Override
    public int getItemCount() {
        if (sekolahList != null)
            return sekolahList.size();
        return 0;
    }


    public interface ISekolahAdapter {
        void doClick(int pos);


        void doShare(int pos);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView ivFoto;
        TextView tvJudul;
        TextView tvDeskripsi;
        ImageButton ibFav, ibShare;

        public ViewHolder(View itemView) {
            super(itemView);
            ivFoto = itemView.findViewById(R.id.imageView);
            tvJudul = itemView.findViewById(R.id.textViewJudul);
            tvDeskripsi = itemView.findViewById(R.id.textViewDeskripsi);

            ibShare = itemView.findViewById(R.id.buttonShare);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mISekolahAdapter.doClick(getAdapterPosition());
                }
            });

            ibShare.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    mISekolahAdapter.doShare(getAdapterPosition());
                }
            });
        }
    }
}
