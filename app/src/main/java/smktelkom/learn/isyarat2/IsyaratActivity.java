package smktelkom.learn.isyarat2;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Locale;

public class IsyaratActivity extends AppCompatActivity {
    private final int REQ_CODE_SPEECH_INPUT = 100;
    Button btnspk, delin;
    TextView suara;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_isyarat);
        btnspk = findViewById(R.id.tombolBicara);
        delin = findViewById(R.id.btndelin);
        suara = findViewById(R.id.sound);
        btnspk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                inputs();
            }
        });

        delin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                suara.setText("");
            }
        });
    }

    private void inputs() {
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
        intent.putExtra(RecognizerIntent.EXTRA_PROMPT,
                "Bicara Sesuatu ");

        try {
            startActivityForResult(intent, REQ_CODE_SPEECH_INPUT);
        } catch (ActivityNotFoundException a) {

        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


        Toast.makeText(this, "Hasil suara ditampilkan", Toast.LENGTH_SHORT).show();
        switch (requestCode) {
            case REQ_CODE_SPEECH_INPUT: {
                if (resultCode == RESULT_OK && null != data) {

                    ArrayList<String> result = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                    suara.setText(result.get(0));
                }
                break;
            }

        }

    }
}

