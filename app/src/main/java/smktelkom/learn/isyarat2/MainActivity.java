package smktelkom.learn.isyarat2;

import android.os.Build;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.Locale;

public class MainActivity extends AppCompatActivity {
    static TextToSpeech tts;
    TextView suara;
    Button btnspk;
    Button spk, delso, delin;
    EditText txt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        spk = findViewById(R.id.buttonSpeak);
        txt = findViewById(R.id.editText);


        delso = findViewById(R.id.DeleteI);


        tts = new TextToSpeech(this, new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                if (status != TextToSpeech.ERROR) {
                    MainActivity.tts.setLanguage(new Locale("id", "ID"));
                } else {
                    tts.setLanguage(Locale.US);
                }
            }
        });
        spk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String text = txt.getText().toString();

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    tts.speak(text, TextToSpeech.QUEUE_FLUSH, null, null);
                } else {
                    tts.speak(text, TextToSpeech.QUEUE_FLUSH, null);
                }
            }
        });

        delso.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                txt.setText("");
            }
        });

    }

}
