package smktelkom.learn.isyarat2;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;

public class MenuActivity extends AppCompatActivity {

    ImageButton bcr, tls, map, ab, isy;
    Button exit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        bcr = findViewById(R.id.bstt);
        exit = findViewById(R.id.bexit);
        tls = findViewById(R.id.btts);
        ab = findViewById(R.id.babout);
        isy = findViewById(R.id.bis);
        map = findViewById(R.id.bmaps);
        bcr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent act2 = new Intent(MenuActivity.this, IsyaratActivity.class);
                startActivity(act2);
            }
        });

        tls.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent tls = new Intent(MenuActivity.this, MainActivity.class);
                startActivity(tls);
            }
        });
        map.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent m = new Intent(MenuActivity.this, MapsActivity.class);
                startActivity(m);
            }
        });
        ab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent ab = new Intent(MenuActivity.this, AboutActivity.class);
                startActivity(ab);
            }
        });
        isy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent is = new Intent(MenuActivity.this, KamusActivity.class);
                startActivity(is);
            }
        });
        exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                moveTaskToBack(true);
            }
        });

    }
}
